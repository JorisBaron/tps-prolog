suspect(I) :- present(I, L, J), vol(L,J,V), a_pu_voler(I,V).

a_pu_voler(I,_) :- sans_argent(I).
a_pu_voler(I,V) :- jaloux(I,V).

vol(hippodrome, lundi, marie).
vol(bar,mardi,jean).
vol(stad,jeudi,luc).

sans_argent(max).
jaloux(eve,marie).

present(max, bar, mercredi).
present(eric, bar, mardi).
present(eve, hippodrome, lundi).