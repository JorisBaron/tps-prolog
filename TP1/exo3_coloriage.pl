couleur(vert).
couleur(jaune).
couleur(rouge).
 
couleur_differente(X, Y) :- X\= Y. 
 
coloriage(C1, C2, C3, C4) :- 
    couleur(C1), couleur(C2), couleur_differente(C1, C2),
    couleur(C3), couleur_differente(C1, C3), couleur_differente(C2, C3),
    couleur(C4), couleur_differente(C1, C4), couleur_differente(C3, C4).