initial(etat(cruche(3,0),cruche(5,0))). 
final(etat(_,cruche(_,4))).


successeur(r1,etat(cruche(V1,C1),cruche(V2,C2)),
                 etat(cruche(V1,V1),cruche(V2,C2))) :-
         C1 < V1.

successeur(r2,etat(cruche(V1,C1),cruche(V2,C2)),
                 etat(cruche(V1,C1),cruche(V2,V2))) :-
         C2 < V2.

successeur(v1,etat(cruche(V1,C1),cruche(V2,C2)),
                etat(cruche(V1,0),cruche(V2,C2))) :-
        C1 > 0.

successeur(v2,etat(cruche(V1,C1),cruche(V2,C2)),
                etat(cruche(V1,C1),cruche(V2,0))) :-
        C2 > 0.

successeur(t12, etat(cruche(V1,C1),cruche(V2,C2)), 
               etat(cruche(V1,C1a),cruche(V2,V2))) :-
         C1 > 0,
         C2 < V2,
         Volume_libre_dans_cruche2 is V2 - C2,
         C1 >= Volume_libre_dans_cruche2,
         C1a is C1 - Volume_libre_dans_cruche2.

successeur(t12,etat(cruche(V1,C1),cruche(V2,C2)),
                 etat(cruche(V1,0),cruche(V2,C2a))) :-
         C1 > 0,
         C2 < V2,
         Volume_libre_dans_cruche2 is V2 - C2,
         C1 < Volume_libre_dans_cruche2,
         C2a is C2 + C1.

successeur(t21,etat(cruche(V1,C1),cruche(V2,C2)),
                 etat(cruche(V1,V1),cruche(V2,C2a))):-
         C2 > 0,
         C1 < V1,
         Volume_libre_dans_cruche1 is V1 - C1,
         C2 >= Volume_libre_dans_cruche1,
         C2a is C2 - Volume_libre_dans_cruche1.

successeur(t21,etat(cruche(V1,C1),cruche(V2,C2)),
                 etat(cruche(V1,C1a),cruche(V2,0))) :-
         C2 > 0,
         C1 < V1,
         Volume_libre_dans_cruche1 is V1 - C1,
         C2 < Volume_libre_dans_cruche1,
         C1a is C1 + C2.



avancer_profondeur(Chemin, Etat, Chemin) :- 
        final(Etat).
avancer_profondeur(Chemin, Etat_actuel, Solution) :-
        successeur(Operation, Etat_actuel, Nouvel_Etat),
        not(member(Nouvel_Etat/_, Chemin)),
        avancer_profondeur([Nouvel_Etat/Operation|Chemin], Nouvel_Etat, Solution).

trouver(Solution) :- 
        initial(Etat_initial), 
        avancer_profondeur([Etat_initial/init], Etat_initial, Solution).

write_list([]).
write_list([H|T]):-
        write(H),nl,
        write_list(T).