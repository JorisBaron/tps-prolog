der(E,[E|[]]).
der(E,[_|M]) :- der(E,M).

avDer(E,[E | [_] ]).
avDer(E,[_|M]) :- avDer(E,M).

longueur([], 0).
longueur([_], 1).
longueur([ _ | Y], N) :- longueur(Y, NN), N is NN+1.

sup_k(H, [H|T], 1, T).
sup_k(X, [H|T], K, [H|R]) :- KK is K-1, sup_k(X, T, KK, R). 

% btw sup_der
sup_der(H,[H],[]).
sup_der(X,[H|T],[H|R]) :- sup_der(X, T, R).

prefixe([], _).
prefixe([H|PT], [H|T]) :- prefixe(PT, T).

suffixe(L,L).
suffixe(S,[_|T]) :- suffixe(S,T).

pair([]).
pair([_| [_|T] ]):-pair(T).

concat([],L2,L2).
concat([H|T],L2,[H|L3]) :- concat(T,L2,L3).

palindrome(X) :- palindrome(X,[]).
palindrome(L,L).
palindrome([_|L],L).
palindrome([H|T],L) :- palindrome(T, [H|L]).

rev([], []).
rev([H|T], L2) :- rev(T, L), concat(L, [H], L2).

revAcc(L, R) :- revAcc(L, R, []).
revAcc([], L, L).
revAcc([H|T], R, Acc) :- revAcc(T, R, [H|Acc]).

%selection(X, L1, L2) :- selection(X, L1, L2, []).
%selection(_, [], []).
selection(X, [X|T1], T1).
selection(X, [H|T1], [H|T2]) :- selection(X, T1, T2).

inter([],_,[]).
inter([H|T1],L2,[H|Int]) :- selection(H, L2, LL2), inter(T1,LL2,Int).
inter([H|T1],L2,Int) :- not(selection(H, L2, _)), inter(T1,L2,Int).

substitution(_,_,[],[]).
substitution(X,Y,[X|L1],[Y|L2]) :- substitution(X,Y,L1,L2).
substitution(X,Y,[H|L1],[H|L2]) :- X \= H, substitution(X,Y,L1,L2).

permut([X],[X]).
permut(LP, [H|T]) :- selection(H, LP, LLP), permut(LLP,T).

permut2([X],[X]).
permut2([H|T],R):- permut2(T,RT),
    concat(L1T,L2T,RT),
    concat(L1T,[H|L2T],R).