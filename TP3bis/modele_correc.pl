valeur(X,X/vrai).
valeur(X,X/faux).

% énumères les interpretation d'une liste de variables.
% valeurs(+Liste, -I)
valeurs([], []).
valeurs([H|T], [HI|TI]) :- valeur(H,HI), valeurs(T, TI).

% Evalue un ensemble de Clause pour une interprétaion donnée
% evalue(+Clause, +Interpretation)
evalue([],_).
evalue([H|T],I) :- evalue_clause(H,I), evalue(T,I).

% Evalue une Clause pour une interprétaion donnée
% evalue_clause(+Clause, +Interpretation)
% evalue_clause([1,2], [vrai, vrai, vrai]).   -> true
% evalue_clause([-1,2], [vrai, vrai, vrai]).  -> true
% evalue_clause([-1,-2], [vrai, vrai, vrai]). -> false
evalue_clause([X|_], I) :- evalue_lit(X, I),!.
evalue_clause([_|T], I) :- evalue_clause(T, I).

% Evalue un littéral pour une interprétation
% evalue_lit(+Litteral, +Interpretation)
evalue_lit(Lit, I) :- Lit > 0,               nth1(Lit , I,  Lit/vrai).
evalue_lit(Lit, I) :- Lit < 0, LitI is -Lit, nth1(LitI, I, LitI/faux).

% Donne les modèles pour un ensemble de clause et un nombre de littéral donné
% modele(+Nb, +Clauses, -Modèle)
modele(N, C, I) :- length(Liste, N), valeurs(Liste, I), evalue(C,I).
