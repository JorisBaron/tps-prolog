% formatte un littéral au format (littéral/valeur)
% format_bool(+Littéral_valué, -Littéral/valeur)
format_bool(Lit, LitI/faux) :- Lit < 0, LitI is -Lit.
format_bool(Lit, Lit/vrai) :- Lit > 0.

% formatte une liste de littéraux au format (littéral/valeur)
% format_bool(+Liste_Littéraux_valués, -Littéral/valeur)
format_liste([],[]).
format_liste([H|T], [HVal|TVal]) :- format_bool(H,HVal), format_liste(T, TVal).

% evalue une clause
% evalue_clause(+Clause, +Interpretation)
evalue_clause([X|_], I) :- member(X, I),!.
evalue_clause([_|T], I) :- evalue_clause(T, I).

% évalue un ensemble de clause
% evalue(+Liste_de_Clauses, +Interpretation)
evalue([],_).
evalue([H|T],I) :- evalue_clause(H,I), evalue(T,I).


% Génère une interprétation et l'évalue
% genere_inter(+Nb_littéraux, -Interpretation)
genere_inter(Nb, I) :- genere_inter(Nb, I, []).

genere_inter(0, I, I) :- !.
genere_inter(Nb, I, Acc) :- Nnb is Nb-1,           genere_inter(Nnb, I, [Nb|Acc]).
genere_inter(Nb, I, Acc) :- Nnb is Nb-1, N is -Nb, genere_inter(Nnb, I, [N|Acc]).  

% détermine le ou les modèle(s) selon la liste de clauses et le nombre de littéraux données, et les formatte
% modele(+Nb_littéraux, +Liste_Clause, -Modèle)
modele(N, Clause, Model) :- genere_inter(N, Inter), evalue(Clause, Inter), format_liste(Inter,Model).
