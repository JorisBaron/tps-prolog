ins(L,L).
ins([],X,[X]).
ins([H|T], X, [X,H|T]) :- X=<H.
ins([H|T],X,[H|Lr]) :- X>H, ins(T,X,Lr).

tri_insert([],[]).
tri_insert([H|T],L) :- tri_insert(T,Lt), ins(Lt,H,L).