minimum2([X],X).
minimum2([H|T],X) :- H < X, minimum2(T,H).
minimum2([H|T],X) :- H >= X, minimum2(T,X).
