longueur([], 0).
longueur([ _ | Y], N) :- longueur(Y, NN), N is NN+1.

diviser(L, L1, L2) :- longueur(L,N), N2 is N/2, diviser(L, L1, L2, 0, N2).
diviser(L, [], L, X, N) :- X >= N.
diviser([H|T], [H|L1], L2, X, N) :- X < N, XX is X+1, diviser(T, L1, L2, XX,N).


combiner([],L,L).
combiner(L,[],L).
combiner([H1|T1],[H2|T2], [H1|Lr]) :- H1<H2, combiner(T1, [H2|T2], Lr).
combiner([H1|T1],[H2|T2], [H2|Lr]) :- H1>=H2, combiner([H1|T1], T2, Lr).

tri_fusion([], []).
tri_fusion([X], [X]).
tri_fusion([H,H2|T], Lr) :- diviser([H,H2|T], L1, L2), tri_fusion(L1, Lr1), tri_fusion(L2, Lr2), combiner(Lr1, Lr2, Lr).