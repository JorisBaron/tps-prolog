minimum([H|T],X) :- minimum(T,H,X).
minimum([],X,X).
minimum([H|T],Min,X) :- H < Min, minimum(T,H,X).
minimum([H|T],Min,X) :- H >= Min, minimum(T,Min,X).

enleve(X,[X|L],L).
enleve(X,[H|L1],[H|L2]) :- X\=H, enleve(X,L1,L2).

tri_min([],[]).
tri_min(L, [X|Lt]) :- minimum(L,X), enleve(X,L,L2), tri_min(L2, Lt).